﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tugas_Day_7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<orang> Person = new List<orang>();
            Person.Add(new orang("Christopher", "Medan", new DateTime(2001, 10, 14), enumethnic.yunani));
            Person.Add(new orang("Ellwan", "Tanjung Pinang", new DateTime(2000, 5, 8), enumethnic.aborigin));
            Person.Add(new orang("Kelvyn", "Surabaya", new DateTime(1997, 10, 26), enumethnic.persia));
            Person.Add(new orang("Donny", "Malang", new DateTime(1989, 1, 30), enumethnic.yunani));
            Person.Add(new orang("Dayton", "Tanjung Pinang", new DateTime(1992, 3, 29), enumethnic.aborigin));
            Person.Add(new orang("Shanata", "Batam", new DateTime(1979, 2, 2), enumethnic.gypsy));
            Person.Add(new orang("Veri", "Malang", new DateTime(1989, 8, 27), enumethnic.yunani));
            Person.Add(new orang("Hosse", "Tanjung Pinang", new DateTime(1998, 5, 7), enumethnic.yunani));
            Person.Add(new orang("Evan", "Malang", new DateTime(1980, 1, 3), enumethnic.aborigin));
            Person.Add(new orang("Frendo", "Bandung", new DateTime(2001, 3, 23), enumethnic.persia));

            List<orang> umur = new List<orang>();
            umur = Person.Where(s => s.age > 30).ToList();

            List<orang> etnis = new List<orang>();
            etnis = Person.Where(s => s.Etnis == enumethnic.yunani).ToList();

            List<orang> umurXetnis = new List<orang>();
            umurXetnis = Person.Where(s => s.age > 30 && s.Etnis == enumethnic.yunani).ToList();

        }
    }
}

