﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tugas_Day_7
{
    public class orang
    {
        public orang(string name, string address, DateTime DateOfBirth, enumethnic etnis)
        {
            nama = name;
            Address = address;
            dateOfBirth = DateOfBirth;
            Etnis = etnis;

        }
        public string nama { get; set; }
        public string Address { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int age
        {
            get
            {
                var today = DateTime.Today;

                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

                return (a - b) / 10000;
            }
        }
        public enumethnic Etnis { get; set; }

    }
    public enum enumethnic
    {
        aborigin = 0,
        gypsy = 1,
        yunani = 2,
        persia = 3,
    }
}
